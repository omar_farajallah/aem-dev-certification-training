package com.slalom.dxm.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.day.cq.wcm.api.PageManager;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;

@Model(adaptables=Resource.class, defaultInjectionStrategy=DefaultInjectionStrategy.OPTIONAL)
public class SummaryPageModel {

    @Inject
    private ResourceResolver resourceResolver;

    @Inject
    private PageManager pageManager;

    @PostConstruct
    protected void init() {
        
    }

}